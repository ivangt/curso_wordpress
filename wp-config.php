<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'negocio' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8,[xF?TX,|!D!2CnGx)`6gWI3 1:CJ`^~ZD$}af}HpkKtvRalzKwTvNpA]io5Gj}' );
define( 'SECURE_AUTH_KEY',  'Vl+~Wy[A=tf);x4:8}_eI)jQ^A{uo8Dd2)Z:{x$sSp8Ihk>>C0^Z#&(A6]b?8pm8' );
define( 'LOGGED_IN_KEY',    'QI_<:`XvV%!2gI0aS3^{nH>_t<a1m8j/zkAz@vM)t2HJxst-.l.&G!P>TT{ BW#P' );
define( 'NONCE_KEY',        'Sd~8?Y?42;W;o)z<H?oTX[+2%(|tZ9.Pp*-C DQ94@{){=A9P1VDOBJT]@3>s=J5' );
define( 'AUTH_SALT',        'hLKVGO5B;f&eKArB8 YcVv0N?^x08_p<XuA1z.sHO$P$HEu:zxu(N`kk+#[ZVXP[' );
define( 'SECURE_AUTH_SALT', 'wOkpZp+$]~&L)C470*Ss80+fp[st{aa:^gTliacLVbBi2<Td7eqWr+%a_||gfru+' );
define( 'LOGGED_IN_SALT',   'RxA%l{?ntJo&nuL2o0Y TL]34z/]p(tE_MP(7Tp}K)K9):3I:ovRoC!vI^*B*Zc4' );
define( 'NONCE_SALT',       'Mb8fTITn:s!f +g)<lH_@;:+tk0[qO3/GKELr[k+U.L11+4aL,ZWJ4@y=|!09SGA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_negocio';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
